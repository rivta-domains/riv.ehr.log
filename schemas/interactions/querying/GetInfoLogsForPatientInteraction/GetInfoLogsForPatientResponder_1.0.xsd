<?xml version="1.0" encoding="UTF-8" ?>
<!-- 
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements. See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership. Inera AB licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License. You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied. See the License for the
 specific language governing permissions and limitations
 under the License.
 -->    
<xs:schema xmlns:xs='http://www.w3.org/2001/XMLSchema' 
    xmlns:tns='urn:riv:ehr:log:querying:GetInfoLogsForPatientResponder:1'                
    xmlns:logquerying='urn:riv:ehr:log:querying:1'
    xmlns:log='urn:riv:ehr:log:1'
    targetNamespace='urn:riv:ehr:log:querying:GetInfoLogsForPatientResponder:1'
    elementFormDefault='qualified'
    attributeFormDefault='unqualified'
    version='1.0'>

    <xs:import schemaLocation='../../../core_components/querying/ehr_logquerying_1.1.xsd' namespace='urn:riv:ehr:log:querying:1' />
    <xs:import schemaLocation='../../../core_components/ehr_log_1.0.xsd' namespace='urn:riv:ehr:log:1' />


    <xs:element name='GetInfoLogsForPatientRequest' type='tns:GetInfoLogsForPatientRequestType'/>

    <xs:complexType name='GetInfoLogsForPatientRequestType'>
        <xs:annotation>
            <xs:documentation xml:lang='sv'>
                Tjänst som returnerar lista för angiven vårdgivare samt patient, vilka vårdgivare som har haft åtkomst till vårdgivarens information där vårdgivaren är informationsägare				 
                Logguttaget begränsas av angivet datumintervall. 
                Tjänsten returnerar en lista med vårdgivare (kan vara noll dvs en tom lista) om resultatkod är OK.		
                Tjänsten returnerar alltid inom 15 sekunder, även ifall rapporten ännu inte har hunnit skapats. Tiden är konfigurerbar av systemet och kan ändras vid behov.
                Om rapporten inte har hunnit skapats av tjänsten returneras ett id (queuedReportId) som identifierar den rapport som håller på att skapas, man får även i detta fall resultkoden REPORTONQUEUE eller REPORTINPROCESS. Man får även en indikation på hur länge det förväntas ta innan rapporten är genererad (queueTime). 
                Med hjälp av queuedReportId skall ytterligare anrop sedan göras av det anropade systemet för att kontrollera/hämta den skapade rapporten. Obeservera att man måste ange queuedReportId, i annat fall kommer en ny rapport att skapas.
                queueTime rekomenderas att användas av det anropande systemet för att bestämma när nästa anrop ska ske.
                VIKTIGT att ytterligare anrop sker med queuedReportId om tidigare anrop avslutats med felkod REPORTONQUEUE eller REPORTINPROCESS för att inte köa upp flera rapporter.
                Tjänsten returnerar statuskod REPORTNOTFOUND ifall man har angett ett felaktigt id(queuedReportId) för att hämta rapport. Ingen ny rapport skapas.
                Tjänsten returnerar max 10000 loggposter. Om fler loggposter finns i rapportuttaget avslutas anropet med felkod MAXQUERYRESULTEXCEEDED. Datumintervall kan då justeras för ett mindra antal loggposter.
                Max antal loggposter som kan returneras är konfigurerbart av systemet och kan ändras vid behov.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="CareProviderId" type="log:HsaId"/>
            <xs:element name="PatientId" type="log:PersonId"/>
            <xs:element name="FromDate" type="xs:dateTime"/>
            <xs:element name="ToDate" type="xs:dateTime"/>
            <xs:element name="QueuedReportId" type="log:Id" minOccurs="0"/>
            <xs:any maxOccurs="unbounded" minOccurs="0" namespace="##other" processContents="lax"/>
        </xs:sequence>
    </xs:complexType>

    <xs:element name='GetInfoLogsForPatientResponse' type='tns:GetInfoLogsForPatientResponseType'/>

    <xs:complexType name='GetInfoLogsForPatientResponseType'>
        <xs:annotation>
            <xs:documentation xml:lang='sv'>
                Tjänst som returnerar lista för angiven vårdgivare samt patient, vilka vårdgivare som har haft åtkomst till vårdgivarens information där vårdgivaren är informationsägare				 
                Logguttaget begränsas av angivet datumintervall. 
                Tjänsten returnerar en lista med vårdgivare (kan vara noll dvs en tom lista) om resultatkod är OK.		
                Tjänsten returnerar alltid inom 15 sekunder, även ifall rapporten ännu inte har hunnit skapats. Tiden är konfigurerbar av systemet och kan ändras vid behov.
                Om rapporten inte har hunnit skapats av tjänsten returneras ett id (queuedReportId) som identifierar den rapport som håller på att skapas, man får även i detta fall resultkoden REPORTONQUEUE eller REPORTINPROCESS. Man får även en indikation på hur länge det förväntas ta innan rapporten är genererad (queueTime). 
                Med hjälp av queuedReportId skall ytterligare anrop sedan göras av det anropade systemet för att kontrollera/hämta den skapade rapporten. Obeservera att man måste ange queuedReportId, i annat fall kommer en ny rapport att skapas.
                queueTime rekomenderas att användas av det anropande systemet för att bestämma när nästa anrop ska ske.
                VIKTIGT att ytterligare anrop sker med queuedReportId om tidigare anrop avslutats med felkod REPORTONQUEUE eller REPORTINPROCESS för att inte köa upp flera rapporter.
                Tjänsten returnerar statuskod REPORTNOTFOUND ifall man har angett ett felaktigt id(queuedReportId) för att hämta rapport. Ingen ny rapport skapas.
                Tjänsten returnerar max 10000 loggposter. Om fler loggposter finns i rapportuttaget avslutas anropet med felkod MAXQUERYRESULTEXCEEDED. Datumintervall kan då justeras för ett mindra antal loggposter.
                Max antal loggposter som kan returneras är konfigurerbart av systemet och kan ändras vid behov.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="InfoLogsResultType" type="logquerying:InfoLogsResultType"/>
            <xs:any maxOccurs="unbounded" minOccurs="0" namespace="##other" processContents="lax"/>
        </xs:sequence>
    </xs:complexType>
</xs:schema>
